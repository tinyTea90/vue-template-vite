/*
 * @Author: 陈铭
 * @Date: 2021-11-22 22:13:05
 * @LastEditors: 陈铭
 * @LastEditTime: 2021-11-23 00:04:01
 * @Description: extend axios with personal options
 * @FilePath: /vue-template-vite/src/utils/http.js
 */
import axios from 'axios';

const http = axios.create({
  baseURL: process.env.STAGING_URL,
  timeout: 600000
});

http.interceptors.request.use((config) => config, (error) => Promise.reject(error));

http.interceptors.response.use((res) => {
  let errorText = '错误';
  if (res.status === 200) {
    const { data: result } = res;
    const { code, data, message } = result;
    if (code === '0000') {
      return Promise.resolve(data);
    }
    errorText = message;
  } else {
    errorText = res.statusText;
  }
  return Promise.reject(errorText);
}, (error) => Promise.reject(error));

export default http;
