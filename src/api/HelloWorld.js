import http from '../utils/http';

export function getHelloWorld() {
  return http.get('/helloWorld', { params: { msg: 'test' } });
}

export function getNewHelloWorld() {
  return http.post('/helloNewWorld', { msg: 'new' });
}
