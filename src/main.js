/*
 * @Author: 陈铭
 * @Date: 2021-09-21 16:56:03
 * @LastEditors: 陈铭
 * @LastEditTime: 2021-11-22 23:48:47
 * @Description: enter file
 * @FilePath: /vue-template-vite/src/main.js
 */
import { createApp } from 'vue';
import App from './App.vue';

createApp(App).mount('#app');
