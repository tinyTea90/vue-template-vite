/*
 * @Author: 陈铭
 * @Date: 2021-11-14 09:41:43
 * @LastEditors: 陈铭
 * @LastEditTime: 2021-11-22 23:37:39
 * @Description: 代码风格配置文件
 * @FilePath: /vue-template-vite/.eslintrc.js
 */
module.exports = {
  env: {
    browser: true,
    es2021: true
  },
  globals: {
    defineProps: 'readonly',
    defineEmits: 'readonly',
    defineExpose: 'readonly',
    withDefaults: 'readonly'
  },
  extends: [
    'airbnb-base',
    'plugin:vue/vue3-essential'
  ],
  parserOptions: {
    ecmaVersion: 12,
    sourceType: 'module'
  },
  plugins: [
    'vue'
  ],
  rules: {
    'comma-dangle': ['error', 'never'],
    'vue/multi-word-component-names': 'off',
    'import/no-extraneous-dependencies': ['error', { devDependencies: true }],
    'no-console': process.env.mode === 'development' ? 2 : 0
  }
};
