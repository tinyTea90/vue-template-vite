/*
 * @Author: 陈铭
 * @Date: 2021-09-21 16:56:03
 * @LastEditors: 陈铭
 * @LastEditTime: 2021-11-22 23:47:59
 * @Description: vite打包配置
 * @FilePath: /vue-template-vite/vite.config.js
 */
import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import dotenv from 'dotenv';
import fs from 'fs';
import { viteMockServe } from 'vite-plugin-mock';

// https://vitejs.dev/config/
export default ({ command, mode }) => {
  console.log(command, mode);
  const envDir = 'config';
  const envFiles = [`${envDir}/.env`, `${envDir}/.env.${mode}`];

  envFiles.forEach((file) => {
    const envConfig = dotenv.parse(fs.readFileSync(file));
    process.env = { ...process.env, ...envConfig };
  });

  let server = {
    host: '0.0.0.0'
  };

  if (command === 'serve' && mode !== 'development') {
    const serverPort = process.env.VITE_SERVER_PORT;
    process.env.STAGING_URL = `http://${process.env.VITE_SERVER_IP}${serverPort ? `:${serverPort}` : serverPort}${process.env.VITE_SERVER_ROOT}`;
    server = {
      ...server,
      proxy: {
        [`${process.env.VITE_BASE_PATH}`]: {
          target: process.env.STAGING_URL,
          changeOrigin: true,
          rewrite: (path) => path.replace(new RegExp(`^${process.env.VITE_BASE_PATH}`), '')
        }
      }
    };
  }

  return defineConfig({
    define: {
      'process.env': process.env
    },
    plugins: [
      vue(),
      viteMockServe({
        prodEnabled: false
      })
    ],
    server
  });
};
