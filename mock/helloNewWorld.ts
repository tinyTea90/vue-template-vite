/*
 * @Author: 陈铭
 * @Date: 2021-10-08 21:18:57
 * @LastEditors: 陈铭
 * @LastEditTime: 2021-11-23 00:07:32
 * @Description: file content
 * @FilePath: /vue-template-vite/mock/helloNewWorld.ts
 */
export default [
  {
    url: '/helloNewWorld',
    method: 'post',
    timeout: 2000,
    response: {
      code: '0100',
      message: '拒绝请求',
      data: {
        name: 'helloNewWorld',
      },
    }
  },
  {
    url: '/helloAnotherWorld',
    method: 'post',
    rawResponse: async (req, res) => {
      let reqbody = ''
      await new Promise((resolve) => {
        req.on('data', (chunk) => {
          reqbody += chunk
        })
        req.on('end', () => resolve(undefined))
      })
      res.setHeader('Content-Type', 'text/plain')
      res.statusCode = 200
      res.end(`hello, ${reqbody}`)
    }
  }
]