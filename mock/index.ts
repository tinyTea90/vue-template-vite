/*
 * @Author: 陈铭
 * @Date: 2021-10-08 21:32:42
 * @LastEditors: 陈铭
 * @LastEditTime: 2021-11-23 00:04:25
 * @Description: file content
 * @FilePath: /vue-template-vite/mock/index.ts
 */
import { MockMethod } from "vite-plugin-mock";
import helloNewWorld from "./helloNewWorld";
import helloWorld from "./helloWorld";

export default [...helloWorld, ...helloNewWorld] as MockMethod[];