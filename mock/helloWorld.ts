/*
 * @Author: 陈铭
 * @Date: 2021-10-08 21:18:44
 * @LastEditors: 陈铭
 * @LastEditTime: 2021-11-22 22:32:44
 * @Description: file content
 * @FilePath: /vue-template-vite/mock/helloWorld.ts
 */
/*
 * @Author: 陈铭
 * @Date: 2021-10-08 21:18:44
 * @LastEditors: 陈铭
 * @LastEditTime: 2021-10-08 21:30:49
 * @Description: file content
 * @FilePath: /vue-template-vite/mock/helloWorld.ts
 */
export default [
  {
    url: '/helloWorld',
    method: 'get',
    response: ({ query }) => {
      console.log('query', query);

      return {
        code: '0000',
        data: {
          name: 'helloWorld',
        },
      }
    }
  }
]